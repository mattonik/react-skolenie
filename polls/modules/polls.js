'use strict';

let mongoose = require('mongoose');
let options = { server: { socketOptions: { keepAlive: 1 } } };

mongoose.connect('mongodb://pollsuser:pollspassword@ds019668.mlab.com:19668/polls', options);
mongoose.connection.on('connected', function() {
	console.log('Mongoose connected');
});
mongoose.connection.on('disconnected', function() {
	console.log('Mongoose disconnected');
});

