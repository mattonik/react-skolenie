const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        bundle: [
            'webpack-hot-middleware/client',
            path.join(__dirname, 'app', 'index.js')
        ]
    },
    output: {
        path: path.join(__dirname, 'public', 'js'),
        publicPath: '/js',
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['react', 'es2015', 'react-hmre']
                }
            }
        ]
    },
    plugins: [
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development'
        }),
        new webpack.HotModuleReplacementPlugin()
    ]
};
