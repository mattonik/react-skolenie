export const POLLS_LOAD_START = 'POLLS_LOAD_START';
export const POLLS_LOAD_SUCCESS = 'POLLS_LOAD_SUCCESS';
export const POLLS_LOAD_FAILURE = 'POLLS_LOAD_FAILURE';

export const VOTE_START = 'VOTE_START';
export const VOTE_SUCCESS = 'VOTE_SUCCESS';
export const VOTE_FAILURE = 'VOTE_FAILURE';

export const FILTER_CHANGE = 'FILTER_CHANGE';

export function loadPolls() {
  return (dispatch, getState) => {
    dispatch({ type: POLLS_LOAD_START });

    return fetch('/api/poll')
      .then(response => response.json())
      .then(polls => {
        dispatch({ type: POLLS_LOAD_SUCCESS, payload: { polls } });
      })
      .catch(err => {
        dispatch({ type: POLLS_LOAD_FAILURE, payload: err, error: true });
      })
  };
}

export function pollVote(pollId, answerId) {
  return (dispatch, getState) => {
    dispatch({ type: VOTE_START });

    const url = `/api/poll/${pollId}/${answerId}`;
    
    return fetch( url, { method: 'PUT' } )
      .then( response => response.json() )
      .then( poll => {
        dispatch({ type: VOTE_SUCCESS, payload: { poll } })
      })
      .catch( err => {
        dispatch({ type: VOTE_FAILURE, payload: err, error: true });
      });
  }
}

export function filterChange(filter) {
  return { type: FILTER_CHANGE, payload: { filter } };
}