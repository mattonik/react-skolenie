export const NAVIGATE_TO_POLL = 'NAVIGATE_TO_POLL';
export const NAVIGATE_TO_LIST = 'NAVIGATE_TO_LIST';

export function navigateToPoll(id) {
  return {
    type: NAVIGATE_TO_POLL,
    payload: { id }
  }
}

export function navigateToList() {
  return {
    type: NAVIGATE_TO_LIST
  }
}