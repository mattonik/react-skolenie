import React, { Component } from 'react';

export default function(prefix) {
  return function(WrappedComponent) {
    class Logger extends Component{
      render() {
        console.log(prefix, 'propsLogger', this.props);
        return <WrappedComponent {...this.props}/>
      }
    }

    let name = ( WrappedComponent.displayName || WrappedComponent.name || 'Component' );
    Logger.displayName = `Logger (${name})`;
    return Logger;
  }
}