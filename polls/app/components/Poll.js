import React, { Component } from 'react';
import PollAnswer from './PollAnswer';
import { connect } from 'react-redux';
import { pollVote } from '../actions/polls';
import { Link } from 'react-router-dom';

class Poll extends Component {
  render() {
    return (
      <div>
        <h1>{this.props.poll.title}</h1>
        {
          this.props.poll.answers.map( a => (
            <PollAnswer answer={a} pollId={this.props.poll.id} key={a.id} onPollVote={(pollId, answerId) => this.props.pollVote(pollId, answerId)}/>
          ))
        }
        <p>
          <Link to="/polls">Späť na zoznam</Link>
        </p>
      </div>
    );
  }
}

const mapState = (state, ownProps) => ({
  poll: state.polls.items.find(p => p.id == ownProps.match.params.id)
});

const mapDispatch = { pollVote }

export default connect(mapState, mapDispatch)(Poll);
