import React, { Component } from 'react';
import { connect } from 'react-redux';
import PollListItem from './PollListItem';
import { filterChange } from '../actions/polls';

class PollList extends Component {
  componentDidMount() {
    this.filterElement.focus();
  }

  render() {
    return (
      <div>
        <h1>Dostupné ankety</h1>

        <input type="search" name="filter" value={this.props.filter} onChange={e => this.props.filterChange(e.target.value)} ref={e => this.filterElement = e} />

        {
          this.props.polls.map( p => <PollListItem poll={p} key={p.id} />)
        }
      </div>
    );
  }
}

const mapState = state => ({
  filter: state.polls.filter,
  polls: state.polls.items.filter(p => !state.polls.filter || p.title.indexOf( state.polls.filter ) >= 0)
});

export default connect(mapState, { filterChange })(PollList);
