import React, { Component } from 'react';
import propsLogger from '../hoc/propsLogger';

class PollHeading extends Component {
  render() {
    console.log('PollHeading render');

    return (
      <div>
        <p>Pocet ankiet: {this.props.polls.length}</p>
      </div>
    );
  }

  shouldComponentUpdate(props) {
    return this.props.polls.length !== props.polls.length;
  }
}

// export default propsLogger(PollHeading);
export default propsLogger('HEADING')(PollHeading);

