import React, { Component } from 'react';

class PollAnswer extends Component {
  render() {
    return (
      <div>
        <h3>{this.props.answer.title} <small>({this.props.answer.votes})</small></h3>
        <button onClick={() => this.props.onPollVote(this.props.pollId, this.props.answer.id)}>Hlasovat</button>
      </div>
    );
  }
}

export default PollAnswer;
