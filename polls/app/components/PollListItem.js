import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// class PollListItem extends Component {
//   render() {
//     return (
//       <div>
//         <h2>{this.props.poll.title}</h2>
//         <button onClick={() => this.props.onNavigateToPoll(this.props.poll.id)}>Vyber anketu</button>
//       </div>
//     );
//   }
// }

// export default PollListItem;

export default ({ poll, onNavigateToPoll }) => <div>
    <h2>{poll.title}</h2>
    <Link to={`/poll/${poll.id}`}>Zobraziť anketu</Link>
  </div>;
