import React, { Component } from 'react';
import PollList from './PollList.js';
import Poll from './Poll.js';
import PollHeading from './PollHeading.js';
import fetch from 'isomorphic-fetch';
import { connect } from 'react-redux';
import { navigateToList, navigateToPoll } from '../actions/navigation';
import { pollVote } from '../actions/polls';
import { Route, Redirect, withRouter } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div>
        <PollHeading polls={this.props.polls.items}/>
        <Route path="/polls" component={PollList}/>
        <Route path="/poll/:id" component={Poll}/>
        <Route exact path="/">
          <Redirect to="/polls"/>
        </Route>
      </div>
    )
  }
}

const mapState = state => state;
const mapDispatch = { navigateToList, navigateToPoll, pollVote }

export default withRouter(connect(mapState, mapDispatch)(App));
