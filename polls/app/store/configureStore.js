import { createStore, applyMiddleware } from 'redux';
import reducer from '../reducers/index';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

export function configureStore() {
  const loggerMiddleware = createLogger();
  const middleware = applyMiddleware( loggerMiddleware, thunkMiddleware );

  const store = createStore( reducer, middleware );

  if(module.hot) {
    module.hot.accept(
      '../reducers/index',
      () => {
        const nextRootReducer =
          require('../reducers').default;
         store.replaceReducer(nextRootReducer)
       }
    )
  }

  return store;
}