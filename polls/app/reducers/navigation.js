import { NAVIGATE_TO_LIST, NAVIGATE_TO_POLL } from '../actions/navigation';

const initialState = { mode: 'LIST', selectedId: null };

export function navigation(state = initialState, action) {
  switch(action.type){
    case NAVIGATE_TO_LIST:
      return Object.assign({}, state, { mode: 'LIST', selectedId: null });
      
    case NAVIGATE_TO_POLL:
      return Object.assign({}, state, { mode: 'ITEM', selectedId: action.payload.id });
  }

  return state;
}