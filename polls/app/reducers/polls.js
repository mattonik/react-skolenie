import { POLLS_LOAD_START, POLLS_LOAD_SUCCESS, POLLS_LOAD_FAILURE, VOTE_START, VOTE_SUCCESS, VOTE_FAILURE, FILTER_CHANGE } from '../actions/polls';

const initiateState = { loading: false, items: [], error: null, filter: '' };

export function polls(state = initiateState, action) {
  switch( action.type ) {
    case POLLS_LOAD_START:
      return Object.assign({}, state, { loading: true, error: null });

    case POLLS_LOAD_SUCCESS:
      return Object.assign({}, state, { loading: false, items: action.payload.polls });

    case POLLS_LOAD_FAILURE:
      return Object.assign({}, state, { loading: false, error: action.payload })

    case VOTE_START:
      return Object.assign({}, state, { voting: true, error: null });

    case VOTE_SUCCESS:
      const poll = action.payload.poll;
      const items = state.items.map(
        p => p.id === poll.id ? poll : p
      )

      return Object.assign({}, state, { voting: false, items });

    case VOTE_FAILURE:
      return Object.assign({}, state, { voting: false, error: action.payload })

    case FILTER_CHANGE:
      return Object.assign({}, state, { filter: action.payload.filter });
  }
  return state;
}