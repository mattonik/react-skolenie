import { combineReducers } from 'redux';
import { navigation } from './navigation';
import { polls } from './polls';

export default combineReducers({ navigation, polls });