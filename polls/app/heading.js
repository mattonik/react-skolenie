import React, { Component } from 'react';

class Heading extends Component {
    render() {
        return <h1>Hello JSX!</h1>
    }
}

export default Heading;