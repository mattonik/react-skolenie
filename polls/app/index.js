import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';
import Heading from './heading';
import App from './components/App';
import { loadPolls } from './actions/polls';
import { BrowserRouter } from 'react-router-dom';

const store = configureStore();

store
  .dispatch( loadPolls() )
  .then( () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <App/>
        </BrowserRouter>
      </Provider>,
      document.getElementById('app')
    )
  });
  