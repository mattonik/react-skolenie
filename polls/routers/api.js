'use strict';

let express = require('express');
let router = express.Router();
let polls = require('../modules/polls');
let Poll = require('../models/poll');

router.put('/poll/:pollId/:answerId', (req, res, next) => {
    Poll.findOne({ id: req.params.pollId }, (err, poll) => {
        if (err) {
            next(err);
        } else {
            if (poll) {
                var answer = poll.answers.find(q => q.id == req.params.answerId);
                if (answer) {
                    answer.votes++;
                    poll.save((err, savedPoll) => {
                        if (err) {
                            next(err);
                        } else {
                            res.json(savedPoll);
                        }
                    });
                } else {
                    res.status(404).json({ error: 'Poll answer not found' });
                }
            } else {

                res.status(404).json({ error: 'Poll not found' });
            }
        }
    });
});

router.get('/poll/:id', (req, res, next) => {
    Poll.findOne({ id: req.params.id }, (err, poll) => {
        if (err) {
            next(err);
        } else {
            if (poll) {
                console.log('poll', poll);
                res.json(poll);
            } else {
                res.status(404).json({ error: 'Poll not found' });
            }
        }
    });
});

router.get('/poll', (req, res, next) => {
    Poll.find({}, (err, polls) => {
        if (err) {
            next(err);
        } else {
            res.json(polls);
        }
    });
});

router.post('/poll', (req, res, next) => {
    if (req.body) {
        if (req.body.title) {
            if (req.body.answers && req.body.answers.length) {
                if (!req.body.answers.filter(q => !q.title).length) {
                    Poll.find({}, (err, polls) => {
                        if (err) {
                            next(err);
                        } else {
                            let maxId = 0;
                            for (let i = 0; i < polls.length; i++) {
                                if (polls[i].id > maxId) {
                                    maxId = polls[i].id;
                                }
                            }

                            let poll = {
                                id: maxId + 1,
                                title: req.body.title,
                                created: new Date(),
                                answers: req.body.answers.map((answer, index) => {
                                    return {
                                        id: index + 1,
                                        title: answer.title,
                                        votes: 0
                                    };
                                })
                            };

                            let pollDocument = new Poll(poll);
                            pollDocument.save((err, poll) => {
                                if (err) {
                                    next(err);
                                } else {
                                    return res.json(poll);
                                }
                            });
                        }
                    });
                } else {
                    res.status(400).json({ error: 'Each poll answer must have a title' });
                }
            } else {
                res.status(400).json({ error: 'Poll must have a list of answers' })
            }
        } else {
            res.status(400).json({ error: 'Poll must have a title' });
        }
    } else {
        res.status(400).json({ error: 'Empty poll cannot be created' });
    }
});

router.use((err, req, res, next) => {
    res.status(500).json({ error: 'Server encoutered an internal error', details: err });
})

module.exports = router; 