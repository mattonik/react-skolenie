'use strict';

let mongoose = require('mongoose');

let schema = new mongoose.Schema({
	id: Number,
	title: String,
    created: Date,
	answers: [{
		id: Number,
		title: String,
		votes: Number
	}]
});
var Model = mongoose.model('Poll', schema);

module.exports = Model;