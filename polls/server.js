'use strict';

let path = require('path');
let webpack = require('webpack');
let webpackDevMiddleware = require('webpack-dev-middleware');
let webpackHotMiddleware = require('webpack-hot-middleware');
let webpackConfig = require('./webpack.config')
let compiler = webpack(webpackConfig);

const port = 8020;
let express = require('express');
let app = express();
let bodyParser = require('body-parser');

app.use(webpackDevMiddleware(compiler, { publicPath: webpackConfig.output.publicPath }));
app.use(webpackHotMiddleware(compiler));
app.use(express.static('public'));
app.use(bodyParser.json());
app.use('/api', require('./routers/api'));

app.use((req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
})

let server = app.listen(port, function() {
    console.log(`Polls started, visit http://localhost:${port}`);
})  